<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

require_once APPPATH . '/modules/api/libraries/REST_Controller.php';

class MyREST_Controller extends REST_Controller {
	
	public $limit;

	public function __construct() {
		parent::__construct(); 
		header('Access-Control-Allow-Origin: *'); 
	} 

}