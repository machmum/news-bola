<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists("instance")){
	function instance() {
		$ci =& get_instance();

		return $ci;
	}
}
