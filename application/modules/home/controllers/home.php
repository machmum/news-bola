<?php if (!defined("BASEPATH")) { exit("No direct script access allowed"); }

class Home extends MY_Controller
{
	private $_active 		= 1;
	private $_limitNews 	= 8;
	private $_offset 		= 0;
	private $_sliderLimit 	= 5;
	private $_newsPublished = 2;

	public function __construct() {
      	parent::__construct();

	    $this->load->model([
	    	"banner_model",
	    	"klasement_model",
	    	"standings/standings_model",
	    	"news/news_model",
	    	"schedule/schedule_model",
	    	"slider_model"
	    ]);
	    $this->load->helper(array('general'));
    }

	public function index()
	{
		$data = $dataHeader = $dataFooter = [];

		/* POPULATE HEADER */
		$dataHeader["menu"] = "home";

		/* POPULATE CONTENT */
		// Get slider
		$whereSlider 	= ["SliderStatus" => $this->_active, "SliderCategory" => $this->_active];
		$getSlider   	= $this->slider_model->getSlider($whereSlider, $this->_sliderLimit, $this->_offset);
		foreach ($getSlider as $value) {
			$value->SliderImage = UPLOAD_SLIDER_PATH . "medium/" . $value->SliderImage;
			$value->SliderURL 	= site_url('news/' . $value->SliderURL);
		}
		
		// R1 R2 banner
		// $whereBanner 	= ["BannerStatus" => $this->_active, "BannerCode" => ["R1", "R2"]];
		$whereBanner 	= ["BannerStatus" => $this->_active, "BannerCategory" => [9]];
		$bannerR 		= $this->banner_model->getBanner($whereBanner, 2, $this->_offset);
		foreach ($bannerR as $value) {
			$value->BannerImage = UPLOAD_BANNER_PATH . "medium/" . $value->BannerImage;
		}

		// Get news
		$whereNews 		= ["NewsStatus" => $this->_newsPublished, "NewsSetHome" => $this->_active];
		$getNews 		= $this->news_model->getNewsHome($whereNews, $this->_offset, $this->_limitNews);
		foreach ($getNews as $key => $value) {
			$value->NewsUrl 			= site_url('news/league/' . slugify($value->LeagueUrl) . "/detail/" . slugify($value->NewsName) . "-" . $value->NewsId);
			$value->NewsImage 			= UPLOAD_NEWS_PATH . "medium/" . $value->NewsImage;
			$value->NewsPublishedTime 	= setPublishedDate($value->NewsPublishedDate);
		}

		// Get leagues
		$whereLeagues 			= ["LeagueStatus" => $this->_active];
		$allLeagues 			= $this->standings_model->getAllLeagues($whereLeagues);
		$getKlasementHightlight = $this->standings_model->getStandings();

		// Get schedule
		$getScheduleHightlight = $this->schedule_model->getSchedules();

		// populate content
		$data["bannerR"] 		= $bannerR ? $bannerR : "";
		$data["listNews"] 		= $getNews ? $getNews : "";
		$data["listSlider"]		= $getSlider ? $getSlider : "";
		$data["listKlasemen"] 	= $getKlasementHightlight ? $getKlasementHightlight : "";
		$data["listJadwal"] 	= $getScheduleHightlight ? $getScheduleHightlight : "";

		/* s: load data to view */
		$view["uri"] 		= "home";
		$view["title"] 		= "News";
		$view["header"] 	= $dataHeader;
		$view["content"] 	= $data;
		$view["footer"] 	= $dataFooter;
		/* e: load data to view */

		$config["content_file"] 	= "index";
		$config["content_data"] 	= $view;

		$this->template->initialize($config);
		$this->template->render();
	}
} 