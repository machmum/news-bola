<div class="content">
	<div class="wrapper">
		<div class="slider flexslider carousel slide-news">
			<?php if (!empty($listSlider)):?>
				<ul class="slides">
					<?php foreach($listSlider as $row => $val):?>
					<li>
						<a href="<?php echo $val->SliderURL;?>">
							<img src="<?php echo $val->SliderImage;?>">
							<h1><?php echo $val->SliderDesc; ?></h1>
						</a>
					</li>
					<?php endforeach;?>
				</ul>
			<?php endif;?>
		</div>
	</div>
</div>

<div class="content main-news-section">
	<div class="wrapper">
		<div class="main-news">
			<?php if(!empty($listNews)) { ?>
				<a href="<?php echo $listNews[0]->NewsUrl; ?>">
					<img src="<?php echo $listNews[0]->NewsImage; ?>">
					<h1><?php echo $listNews[0]->NewsTeaser; ?></h1>
				</a>
			<?php } ?>
		</div>
		<?php if (!empty($bannerR)) { ?>
		<div class="main-ads" style="display:block !important">
			<?php foreach ($bannerR as $value) { ?>
				<div class="ads-item" style="display: block !important"><img src="<?php echo $value->BannerImage; ?>" style="width: inherit;"></div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>

<div class="content news-list">
	<div class="wrapper">
		<div class="news-left">
			<div class="klasement-wrapper">
				<h1 class="title-section">Klasemen</h1>
				<?php if(!empty($listKlasemen)) { ?>
				<div class="flexslider99">
					<ul class="slides">
						<?php foreach ($listKlasemen as $key => $value) { ?>
							<li>
								<div class="klasemen">
									<h1><?php echo $value->name; ?></h1>
									<ul>
										<li class="table-header">
											<span>Posisi</span>
											<span>Tim</span>
											<span>Poin</span>
										</li>
										<?php foreach ($value->team as $team) { ?>
											<li>
												<span><?php echo $team->position; ?></span>
												<span><img src="<?php echo $team->img; ?>"> <?php echo $team->name;?></span>
												<span><?php echo $team->poin;?></span>
											</li>
										<?php } ?>
									</ul>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
			</div>
			<?php if(!empty($listJadwal)) { ?>
			<div class="klasement-wrapper">
				<h1 class="title-section">jadwal</h1>
				<div class="flexslider99">
					<ul class="slides">
						<?php foreach ($listJadwal as $key => $value) { ?>
							<li>
								<div class="jadwal">
									<h1><?php echo $value->name; ?></h1>
									<ul>
										<li>
											<div class="jadwal-liga">
											<?php foreach ($value->teams as $teams) { ?>
												<p><?php echo $teams->date; ?></p>
												<ul>
													<?php foreach ($teams->detail as $flag => $detail) { ?>
													<li>
														<span class="time <?php echo ($flag%2 == 0) ? '' : 'ft'; ?>">ft</span>
														<span class="home"><?php echo $detail->team[0] . "<p>" .$detail->score[0]. "</p>"; ?> </span>
														<span class="separator"> - </span>
														<span class="away"><?php echo "<p>" .$detail->score[1]. "</p>" . $detail->team[1]; ?> </span>
													</li>
													<?php } ?>
												</ul>
											<?php } ?>
											</div>
										</li>
									</ul>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<?php } ?>

		</div>
		<div class="news-right">
			<div class="news-list-wrapper">
				<?php if(!empty($listNews)) { ?>
				<ul>
					<?php foreach ($listNews as $key => $val) { ?>
						<?php if ($key == 0) continue; ?>
						<li>
							<a href="<?php echo $val->NewsUrl;?>">
								<img src="<?php echo $val->NewsImage; ?>">
								<div class="news-content">
									<h1><?php echo $val->NewsName ?></h1>
									<p><?php echo $val->NewsDesc ?></p>
									<div class="news-stat">
										<ul>
											<li>
												<img src="<?php echo base_url(); ?>assets/img/view.svg">
												<span><?php echo $val->NewsViews ?> views</span>
											</li>
											<li>
												<img src="<?php echo base_url(); ?>assets/img/clock.svg">
												<span><?php echo $val->NewsPublishedTime; ?></span>
											</li>
										</ul>
									</div>
								</div>
							</a>
						</li>	
					<?php } ?>
				</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</div>