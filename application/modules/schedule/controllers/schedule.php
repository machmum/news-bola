<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Schedule extends MY_Controller
{
	private $_active 		= 1;
	private $_offset 		= 0;

	public function __construct() {
      	parent::__construct();

	    $this->load->model([
	    	'banner_model',
	    	'schedule_model',
	    ]);
    }

	public function index()
	{
		$data = $dataHeader = $dataFooter = [];
		
		/* POPULATE HEADER */
		$dataHeader["menu"] = "home";

		/* POPULATE CONTENT */
		// Get getSchedules
		$getSchedules = $this->schedule_model->getSchedules();

		// R1 R2 banner
		$whereBanner 	= ["BannerStatus" => $this->_active, "BannerCode" => ["R1", "R2"]];
		$bannerR 		= $this->banner_model->getBanner($whereBanner, 2, $this->_offset);
		foreach ($bannerR as $value) {
			$value->BannerImage = UPLOAD_BANNER_PATH . $value->BannerImage;
			$value->BannerUrl 	= site_url('news/' . $value->BannerUrl);
		}

		// populate content
		$data['schedules'] 	= $getSchedules ? $getSchedules : '';
		$data["bannerR"] 	= $bannerR ? $bannerR : "";

		/* s: load data to view */
		$view["uri"] 		= "schedules";
		$view["title"] 		= "Schedules";
		$view["header"] 	= $dataHeader;
		$view["content"] 	= $data;
		$view["footer"] 	= $dataFooter;
		/* e: load data to view */

		$config['content_file'] = "index";
		$config["content_data"] = $view;

		$this->template->initialize($config);
		$this->template->render();
	}

} 