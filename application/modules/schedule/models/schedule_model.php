<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_model extends MY_Model {

	private $_tabel = 'Schedule';
	private $_klasement = 'Klasement';

	private $_activeStatus = 1;
	
	public function __construct(){
		parent::__construct();
	}

	public function getAllLeagues($id = NULL) 
	{
		$where = ["Status" => $this->_activeStatus];
		return $this->get_all($this->_tabel,"","",$where);
	}

	public function getSchedules()
	{
		$return = [];

		try {
			$this->db->select("*", false);
			$this->db->from($this->_tabel);
			$this->db->where("ScheduleStatus", $this->_activeStatus);
			$this->db->order_by("ScheduleDate DESC");

			$query = $this->db->get();

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->result();
			foreach ($result as $key => $value) {
				$id 	= $value->ScheduleLeague;
				$sch 	= strtotime($value->ScheduleDate);
				$return[$id]["id"] 		= $value->ScheduleLeague;
				$return[$id]["name"] 	= $value->ScheduleName;
				$return[$id]["teams"][$sch]["date"] 	= date("l, d F Y", $sch);
				$return[$id]["teams"][$sch]["detail"][] = [
					"team" => [
						$value->ScheduleTeam1,
						$value->ScheduleTeam2,
					],
					"score" => [
						$value->ScheduleScore1,
						$value->ScheduleScore2,
					],
				];
			}
			// print_r($return);die;
			
			return json_decode(json_encode($return), false);
		} catch (Exception $e) {
			
		}
	}

	public function getLeagueHightlight()
	{
		$return = [];
		$this->db->select("
				l.LeagueId,
				l.LeagueName,
				k.KlasementId,
				k.KlasementTeam,
				k.KlasementPoints,
				k.KlasementPosition,
				k.KlasementImg
				", false);
			$this->db->from("{$this->_tabel} l");
			$this->db->join("{$this->_klasement} k", "l.LeagueId = k.KlasementLeague", "LEFT");
			$this->db->where("l.Status", $this->_activeStatus);
			$this->db->where("k.KlasementStatus", $this->_activeStatus);
			$this->db->order_by("l.LeagueId, k.KlasementPosition ASC");

		$query = $this->db->get();

		if ($query === false) {
			throw new Exception();
		}

		$result = $query->result();
		foreach ($result as $key => $value) {
			$id 	= $value->LeagueId;
			$idK 	= $value->KlasementId;
			$return[$id]["id"] 		= $value->LeagueId;
			$return[$id]["name"] 	= $value->LeagueName;
			$return[$id]["team"][$idK] 	= [
				"name" 		=> $value->KlasementTeam,
				"position" 	=> $value->KlasementPosition,
				"poin" 		=> $value->KlasementPoints,
				"img" 		=> $value->KlasementImg,
			];

			$return[$id]["team"] = array_values($return[$id]["team"]);
		}

		$return = array_values($return);

		return $return;
	}

}