<div class="content">
	<div class="wrapper">
		<div class="hasil-wrapper">
			<div class="hasil-left">
				<h1 class="title-section">JADWAL</h1>
				<?php if (!empty($schedules)) { ?>
					<?php foreach ($schedules as $value) { ?>
						<div class="jadwal">
						<h1><?php echo $value->name; ?></h1>
						<ul>
							<li>
								<div class="jadwal-liga">
								<?php foreach ($value->teams as $teams) { ?>
									<p><?php echo $teams->date; ?></p>
									<ul>
										<?php foreach ($teams->detail as $flag => $detail) { ?>
										<li>
											<span class="time <?php echo ($flag%2 == 0) ? 'ft' : ''; ?>">ft</span>
											<span class="home"><?php echo $detail->team[0] . "<p>" .$detail->score[0]. "</p>"; ?> </span>
											<span class="separator"> - </span>
											<span class="away"><?php echo "<p>" .$detail->score[1]. "</p>" . $detail->team[1]; ?> </span>
										</li>
										<?php } ?>
									</ul>
								<?php } ?>
								</div>
							</li>
						</ul>
					</div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php if (!empty($bannerR)) { ?>
			<div class="hasil-right" style="display:block !important">
				<?php foreach ($bannerR as $value) { ?>
					<div class="ads-item" style="display: block !important"><img src="<?php echo $value->BannerImage; ?>" style="width: inherit;"></div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>