<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class News extends MY_Controller
{
	private $_active 		= 1;
	private $_offset 		= 0;
	private $_limitNews 	= TOTAL_ITEM_PER_PAGE;

	public function __construct() {
      	parent::__construct();

	    $this->load->model([
	    	'news_model',
	    	'banner_model',
	    	'standings/standings_model',
	    ]);

	    $this->load->helper(array('general'));
    }

	public function index($filterby = "all", $filtername = "all", $page = 1)
	{
		// init data
		$data = $dataHeader = $dataFooter = [];

		// pagination
		$page        	= ($page < 1) ? 1 : ($page - 1);
		$this->_offset 	= $page * $this->_limitNews;

		// sort & filter
		$sort = $this->input->get("sort");
		$filter = [
			"filterby" => $filterby,
			"filtername" => $filtername,
		];

		// search
		$search = $this->input->get('search');
		// print_r($search);die;

		/* POPULATE HEADER */
		$dataHeader["menu"] = "home";

		/* POPULATE CONTENT */
		$getNews 	= $this->news_model->getNews($search, $this->_offset, $this->_limitNews, $filter, $sort);
		$totalNews 	= $this->news_model->getTotalNews($search, $filter, $sort);
		foreach ($getNews as $key => $value) {
			$value->NewsUrl 			= site_url('news/league/' . slugify($value->LeagueUrl) . "/detail/" . slugify($value->NewsName) . "-" . $value->NewsId);
			$value->NewsImage 			= UPLOAD_NEWS_PATH . "medium/" . $value->NewsImage;
			$value->NewsPublishedTime 	= setPublishedDate($value->NewsPublishedDate);
		}
		// print_r($getNews);die;

		$whereClub 		= ["KlasementStatus" => $this->_active];
		$club 			= $this->standings_model->getAllKlasement($whereClub);
		
		$whereLeague 	= ["LeagueStatus" => $this->_active];
		$league 		= $this->standings_model->getAllLeagues($whereLeague);

		// pagination init
		$baseUrl 		= site_url('news');
		$params 		= $_SERVER["QUERY_STRING"];
		$baseUrl 		= $filterby != "all" ? base_url() . "news/{$filterby}/{$filtername}" : $baseUrl;
		$uriSegment 	= $filterby != "all" ? 4 : 2;
		$suffix 		= !empty($params) ? "?" . $params : "";

		$config = set_config_pagination($baseUrl, $suffix, $uriSegment, $totalNews, $this->_limitNews);
		$this->pagination->initialize($config);

		// populate content
		$data['news'] 	= $getNews ? $getNews : '';
		$data['club'] 	= $club ? $club : '';
		$data['league'] = $league ? $league : '';
		$data['baseUrl'] = $baseUrl;
		$data["pagination"] = $this->pagination->create_links();
		// print_r($data);die;

		/* s: load data to view */
		$view["uri"] 		= "news";
		$view["title"] 		= "News";
		$view["header"] 	= $dataHeader;
		$view["content"] 	= $data;
		$view["footer"] 	= $dataFooter;
		/* e: load data to view */

		$config['content_file'] = "index";
		$config["content_data"] = $view;

		$this->template->initialize($config);
		$this->template->render();
	}

	public function detail($uri)
	{
		$data = $dataHeader = $dataFooter = [];

		/* POPULATE HEADER */
		$dataHeader["menu"] = "home";

		/* POPULATE CONTENT */
		// Get banner
		$whereBanner1 	= ["BannerStatus" => $this->_active, "BannerCategory" => [8]];
		$getMainBanner  = $this->banner_model->getBanner($whereBanner1, 4, $this->_offset);
		foreach ($getMainBanner as $value) {
			$value->BannerImage = UPLOAD_BANNER_PATH . 'large/' . $value->BannerImage;
		}

		$whereBanner2 	= ["BannerStatus" => $this->_active, "BannerCategory" => [9]];
		$bannerR 		= $this->banner_model->getBanner($whereBanner2, 2, $this->_offset);
		foreach ($bannerR as $value) {
			$value->BannerImage = UPLOAD_BANNER_PATH . 'medium/' . $value->BannerImage;
			$value->BannerUrl 	= site_url('news/' . $value->BannerUrl);
		}

		// Get detail news
		$uri = strtolower(trim($uri));
		$id = substr($uri, strrpos($uri, '-') + 1);
		// insert visit
		$this->news_model->insertNewsVisit($id);
		$getDetailNews 	= $this->news_model->getDetailNews($id);

		// populate content
		$data['detail'] 		= $getDetailNews ? $getDetailNews : '';
		$data['relatedNews'] 	= !empty($getDetailNews->NewsRelated) ? $getDetailNews->NewsRelated : '';
		$data['relatedProfile']	= !empty($getDetailNews->RelatedProfile) ? $getDetailNews->RelatedProfile : '';
		$data['banner'] 		= $getMainBanner ? $getMainBanner : '';
		$data['bannerR'] 		= $bannerR ? $bannerR : '';

		/* s: load data to view */
		$view["uri"] 		= "newsDetail";
		$view['title'] 		= 'News | ' . !empty($getDetailNews->NewsName) ? $getDetailNews->NewsName : '';
		$view["header"] 	= $dataHeader;
		$view["content"] 	= $data;
		$view["footer"] 	= $dataFooter;
		/* e: load data to view */

		$config['content_file'] = "detail";
		$config["content_data"] = $view;

		$this->template->initialize($config);
		$this->template->render();
	}

} 