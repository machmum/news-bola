<div class="content">
	<div class="wrapper">
		<div class="news-banner-slider flexslider-news">
			<?php echo $this->load->view('banner/banner',$banner,TRUE); ?>
		</div>
		<div class="content-news">
			<div class="news-left">
				<div class="news-left-wrapper">
					<?php if (!empty($detail)) { ?>
						<div class="news-wrapper">
							<div class="news-header">
								<p><?php echo $detail->NewsPublishedDate; ?></p>
								<h1><?php echo $detail->NewsName; ?></h1>
								<ul>
									<li><?php echo !empty($detail->UserLoginName) ? $detail->UserLoginName : ''; ?></li>
									<li><img src="<?php echo base_url(); ?>assets/img/view.svg"><span><?php echo $detail->NewsViews; ?></span></li>
								</ul>
							</div>
						</div>
						<div class="news-detail-image">
							<img src="<?php echo $detail->NewsImage; ?>">
							<p><?php echo $detail->NewsDesc; ?></p>
						</div>
						<div class="news-wrapper news-stories">
							<p><?php echo $detail->NewsDesc; ?></p>
						</div>
					<?php } ?>
				</div>
				<div class="other-news">
					<?php if (!empty($relatedNews)) { ?>
						<div class="other-left berita-lainnya">
							<h1 class="title-section">berita lainnya</h1>
							<ul>
								<?php foreach ($relatedNews as $value) { ?>
								<li>
									<a href="<?php echo isset($value[3]) ? $value[3] : ''; ?>">
										<img src="<?php echo $value[2]; ?>" style="width: 100px !important;height:65 !important">
										<h1><?php echo $value[1]; ?></h1>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
					<?php if (!empty($relatedProfile)) { ?>
						<div class="other-right related">
							<h1 class="title-section">related profile</h1>
							<ul>
								<?php foreach ($relatedProfile as $value) { ?>
								<li>
									<a href="<?php echo site_url('profile/' . $value->ProfileUrl); ?>">
										<img src="<?php echo $value->ProfileImage; ?>">
										<h1><?php echo $value->ProfileName; ?> <span><img src="https://ls.sportradar.com/ls/crest/medium/2829.png"></span></h1>
									</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</div>
			</div>
			<?php if (!empty($bannerR)) { ?>
			<div class="news-right" style="display:block !important">
				<?php foreach ($bannerR as $value) { ?>
					<div class="ads-item" style="display: block !important"><img src="<?php echo $value->BannerImage; ?>" style="width: inherit;"></div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>