<div class="content">
	<div class="wrapper">
		<h1 class="title-section filter-title">BERITA <span>FILTER</span></h1>
		<div class="berita-wrapper">
			<div class="filter">
				<div class="search">
					<input type="text" id="searchval" name="" placeholder="search...">
					<i class="fa fa-search"></i>
				</div>
				<div class="sort-by" id="sortby">
					<h1 class="filter-title">Sort By</h1>
				 	<ul>
				 		<li>
							<label>
								<input class="view" type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_0" data-sort="view">
								<span>Top Viewed</span>
							</label>
				 		</li>
				 		<li>
							<label>
								<input class="newest" type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_1" data-sort="newest">
								<span>Newest</span>
							</label>
				 		</li>
				 		<li>
							<label>
								<input class="title" type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_2" data-sort="title">
								<span>A to Z</span>
							</label>
				 		</li>
				 	</ul>
                </div>
                <div class="filter-content" id="filterby">
                	<h1 class="filter-title">Filter By</h1>
                	<ul>
                		<?php if (!empty($league)) { ?>
                			<li class="filterby" data-filterby="league">
	                			<a href="javascript:void(0)"><i class="fa fa-angle-right"></i>League</a>
	                			<div>
	                				<ul>
	                					<?php foreach ($league as $value) { ?>
	                						<li class="fl"><a href="javascript:void(0)" data-filter="<?php echo strtolower($value->LeagueUrl); ?>"><?php echo $value->LeagueName; ?></a></li>
	                					<?php } ?>
	                				</ul>
	                			</div>
	                		</li>
                		<?php } ?>

                		<?php if (!empty($club)) { ?>
                			<li class="filterby" data-filterby="club">
	                			<a href="javascript:void(0)"><i class="fa fa-angle-right"></i>Club</a>
	                			<div>
	                				<ul>
	                					<?php foreach ($club as $value) { ?>
	                						<li class="fl"><a href="javascript:void(0)" data-filter="<?php echo strtolower($value->KlasementTeam); ?>"><?php echo $value->KlasementTeam; ?></a></li>
	                					<?php } ?>
	                				</ul>
	                			</div>
	                		</li>
                		<?php } ?>

                	</ul>
                </div>
			</div>
			<div class="news-list">
				<?php if (!empty($news)) { ?>
				<div class="news-list-wrapper">
					<ul>
					<?php foreach ($news as $key => $val) { ?>
						<li>
							<a href="<?php echo $val->NewsUrl;?>">
								<img src="<?php echo $val->NewsImage; ?>">
								<div class="news-content">
									<h1><?php echo $val->NewsName ?></h1>
									<p><?php echo $val->NewsDesc ?></p>
									<div class="news-stat">
										<ul>
											<li>
												<img src="<?php echo base_url(); ?>assets/img/view.svg">
												<span><?php echo $val->NewsViews ?> views</span>
											</li>
											<li>
												<img src="<?php echo base_url(); ?>assets/img/clock.svg">
												<span><?php echo $val->NewsPublishedTime; ?></span>
											</li>
										</ul>
									</div>
								</div>
							</a>
						</li>	
					<?php } ?>
				</ul>
				</div>
				<?php } else { ?>
				<div class="news-list-wrapper">
					<p>Tidak ada hasil</p>
				</div>
				<?php } ?>
				<div class="paginate">
					<?php echo $pagination; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
var redirect, sort, filterby, filter;
var q = location.search;
var base = location.pathname;
var segment = base.split("/").length - 1 - (base.indexOf("http://")==-1?0:2);

$(document).ready(function(){
	console.log(q);
	if (q != "") {
		var cek = q.substring(1,7);
		if (cek != "search") {
			sort = q.substring(6);
			$("."+sort).attr('checked', 'checked');	
		}
	}

	$("#sortby input").on("click", function(){
		sort = $(this).attr('data-sort');
		base = segment < 3 ? "<?php echo $baseUrl; ?>" : "<?php echo $baseUrl; ?>";
		redirect = base + "?sort=" + sort;

		window.location.replace(redirect);
	});

	$("#filterby .fl").on("click", function(){
		filterby = $(this).parent().parent().parent(".filterby").attr('data-filterby');
		filter = $(this).children().attr('data-filter');
		base = "<?php echo site_url('news'); ?>";
		redirect = base + "/" + filterby + "/" + filter;

		window.location.replace(redirect);
	});

	$(".search").keyup(function(e) {
		var keyCode = (window.event) ? e.which : e.keyCode;
		var search = $(this).children('input').val();
		search = escapeHtml(search.trim())
		redirect = "<?php echo site_url('news'); ?>" + "?search=" + search;

		if (keyCode == 13) {
			window.location.replace(redirect);
		}
	});

	function escapeHtml(unsafe) {
	    return unsafe
	         .replace(/&/g, "&amp;")
	         .replace(/</g, "&lt;")
	         .replace(/>/g, "&gt;")
	         .replace(/"/g, "&quot;")
	         .replace(/'/g, "&#039;");
	 }
});
</script>