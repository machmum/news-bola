<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends MY_Model {

	private $_tabel = 'News';
	private $_user 	= 'UserLogin';
	private $_profile = 'Profile';
	private $_league = "League";
	private $_klasement = "Klasement";

	private $_activeStatus 	= 1;
	private $_newsPublished = 2;
	
	public function __construct(){
		parent::__construct();

		$this->load->model([
			'profile_model'
	    ]);
	}

	public function getDetailNews($id) {
		$result = [];
		$where 	= ["NewsId" => $id];

		try {
			$this->db->select("n.*, u.UserLoginName,
				(SELECT GROUP_CONCAT(CONCAT_WS('_',NewsId,NewsName,NewsImage) SEPARATOR ',')
					FROM News
					WHERE NewsLeague = n.NewsLeague
					AND NewsStatus = {$this->_newsPublished}
					LIMIT 8
				) as NewsRelated,
				l.LeagueUrl
				", false);
			$this->db->from("{$this->_tabel} n");
			$this->db->join("{$this->_user} u" , "u.UserLoginId = n.NewsCreatedBy", "left");
			$this->db->join("{$this->_league} l" , "l.LeagueId = n.NewsLeague", "left");
			$this->db->where($where);
			$this->db->where("n.NewsStatus", $this->_newsPublished);

			$query = $this->db->get();
			// print_r($this->db->last_query());die;

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->row();

			if ($result) {
				$result->NewsImage 			= UPLOAD_NEWS_PATH . 'large/' . $result->NewsImage;
				$result->NewsPublishedDate 	= convert_date(24, $result->NewsPublishedDate);
				$result->NewsRelated 		= explode(",", str_replace(" ","",$result->NewsRelated));
				$result->RelatedProfile 	= unserialize($result->RelatedProfile);
				$result->RelatedProfile 	= $this->profile_model->getProfiles(["ProfileId" => $result->RelatedProfile]);

				foreach ($result->RelatedProfile as $value) {
					$value->ProfileImage = UPLOAD_PROFILE_PATH . 'thumb/' . (isset($value->ProfileImage) ? $value->ProfileImage : "");
				}

				foreach ($result->NewsRelated as $key => $value) {
					// due to bad admin
					// heres related news
					// 0 => NewsId
					// 1 => NewsName
					// 2 => NewsImage
					// 3 => NewsUrl
					$result->NewsRelated[$key] = explode("_", $value);
					$result->NewsRelated[$key][2] = UPLOAD_NEWS_PATH . 'thumb/' . (isset($result->NewsRelated[$key][2]) ? $result->NewsRelated[$key][2] : "");
					$newsUrl = site_url('news/league/' . slugify($result->LeagueUrl) . "/detail/" . slugify($result->NewsRelated[$key][1]) . '-' . $result->NewsRelated[$key][0]);
					array_push($result->NewsRelated[$key], $newsUrl);
				}
			}

			return $result;
		} catch (Exception $e) {
			
		}
	}



	public function getNewsHome($where = "", $start_limit = "", $end_limit = "") {

		$now = date('Y-m-d H:i:s');

		try {
			$this->db->select("n.*, l.LeagueUrl", false);
			$this->db->from("{$this->_tabel} n");
			$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague");
			$this->db->where("NewsPublishedDate <=", $now);
			$this->db->order_by("NewsPublishedDate DESC");

			// limit and offset
			if ($start_limit != "" or $end_limit != "")
				$this->db->limit($end_limit, $start_limit);

			$query = $this->db->get();

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->result();

			return $result;
		} catch (Exception $e) {
			
		}
	}

	public function getNews($search = "", $start_limit = "", $end_limit = "", $filter = [], $sort = "") {

		$now = date('Y-m-d H:i:s');

		try {
			$filterby = $filter["filterby"];
			$filtername = $filter["filtername"];

			$this->db->select("n.*, l.LeagueUrl", false);
			$this->db->from("{$this->_tabel} n");
			if ($filterby == "league") {
				$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague AND l.LeagueUrl = '{$filtername}'");
			} else {
				$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague");
			}
			// if ($filterby != "all") {
			// 	if ($filterby == "league") {
			// 		$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague AND l.LeagueUrl = '{$filtername}'");
			// 	}
			if ($filterby == "club") {
				$this->db->join("{$this->_klasement} k", "k.KlasementId = n.NewsClub AND k.KlasementTeam like '%{$filtername}%'");
			}
			// }
			
			if ($search != "") {
				$this->db->where("NewsName like", "%$search%");
			}
			$this->db->where("NewsPublishedDate <=", $now);

			if (!empty($sort)) {
				if ($sort == "view") {
					$this->db->order_by("NewsViews DESC");
				}
				else if ($sort == "newest") {
					$this->db->order_by("NewsPublishedDate DESC");
				}
				else if ($sort == "title") {
					$this->db->order_by("NewsName ASC");
				}
			} else {
				$this->db->order_by("NewsPublishedDate DESC");
				// $this->db->order_by("NewsPublishedDate", "DESC");
			}

			// limit and offset
			if ($start_limit != "" or $end_limit != "")
				$this->db->limit($end_limit, $start_limit);

			$query = $this->db->get();

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->result();

			return $result;
		} catch (Exception $e) {
			
		}
	}

	public function getTotalNews($search = "", $filter = [], $sort = "") {

		$now = date('Y-m-d H:i:s');

		try {
			$filterby = $filter["filterby"];
			$filtername = $filter["filtername"];

			$this->db->select("n.*, l.LeagueUrl", false);
			$this->db->from("{$this->_tabel} n");
			if ($filterby == "league") {
				$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague AND l.LeagueUrl = '{$filtername}'");
			} else {
				$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague");
			}
			// if ($filterby != "all") {
			// 	if ($filterby == "league") {
			// 		$this->db->join("{$this->_league} l", "l.LeagueId = n.NewsLeague AND l.LeagueUrl = '{$filtername}'");
			// 	}
			if ($filterby == "club") {
				$this->db->join("{$this->_klasement} k", "k.KlasementId = n.NewsClub AND k.KlasementTeam like '%{$filtername}%'");
			}
			// }
			
			if ($search != "") {
				$this->db->where("NewsName like", "%$search%");
			}
			$this->db->where("NewsPublishedDate <=", $now);

			if (!empty($sort)) {
				if ($sort == "view") {
					$this->db->order_by("NewsViews DESC");
				}
				else if ($sort == "newest") {
					$this->db->order_by("NewsPublishedDate DESC");
				}
				else if ($sort == "title") {
					$this->db->order_by("NewsName ASC");
				}
			} else {
				$this->db->order_by("NewsPublishedDate", "DESC");
			}

			$query = $this->db->get();

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->num_rows();

			return $result;
		} catch (Exception $e) {
			
		}
	}

	public function insertNewsVisit($id)
	{
		$this->db->where("NewsId", $id);
		$this->db->set("NewsViews", "NewsViews+1", false);
		$update = $this->db->update("News");

		if ($update === false)
			throw new Exception();

		return true;
	}

}