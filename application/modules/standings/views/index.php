<div class="content">
	<div class="wrapper">
		<div class="klasemen-wrapper">
			<div class="klasemen-left">
				<div class="klasemen">
					<h1 class="title-section">KLASEMENT</h1>
				</div>
				<?php if (!empty($standings)) { ?>
					<?php foreach ($standings as $std) { ?>
					<div class="klasemen">
						<div class="klasemen-table-wrapper">
							<h1 class="klasemen-title"><?php echo $std->name; ?></h1>
							<ul class="klasemen-table">
								<li class="table-head">
									<div>posisi</div>
									<div>tim</div>
									<div>p</div>
									<div>m</div>
									<div>s</div>
									<div>k</div>
									<div>+/-</div>
									<div>pts</div>
								</li>
								<?php foreach ($std->team as $detail) { ?>
									<li class="table-content">
										<div><?php echo $detail->position; ?></div>
										<div><img src="<?php echo $detail->img; ?>"> <?php echo $detail->name; ?></div>
										<div><?php echo $detail->game; ?></div>
										<div><?php echo $detail->win; ?></div>
										<div><?php echo $detail->draw; ?></div>
										<div><?php echo $detail->lost; ?></div>
										<div><?php echo $detail->deficit; ?></div>
										<div><?php echo $detail->poin; ?></div>
									</li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php if (!empty($bannerR)) { ?>
			<div class="klasemen-right" style="display:block !important">
				<?php foreach ($bannerR as $value) { ?>
					<div class="ads-item" style="display: block !important"><img src="<?php echo $value->BannerImage; ?>" style="width: inherit;"></div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>