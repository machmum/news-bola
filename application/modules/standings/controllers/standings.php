<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Standings extends MY_Controller
{
	private $_active 		= 1;
	private $_offset 		= 0;

	public function __construct() {
      	parent::__construct();

	    $this->load->model([
	    	'banner_model',
	    	'standings_model',
		]);
    }

	public function index()
	{
		$data = $dataHeader = $dataFooter = [];
		
		/* POPULATE HEADER */
		$dataHeader["menu"] = "home";

		/* POPULATE CONTENT */
		// Get leagues
		$getKlasementHightlight = $this->standings_model->getStandings();

		// R1 R2 banner
		$whereBanner 	= ["BannerStatus" => $this->_active, "BannerCode" => ["R1", "R2"]];
		$bannerR 		= $this->banner_model->getBanner($whereBanner, 2, $this->_offset);
		foreach ($bannerR as $value) {
			$value->BannerImage = UPLOAD_BANNER_PATH . $value->BannerImage;
			$value->BannerUrl 	= site_url('news/' . $value->BannerUrl);
		}

		// populate content
		$data['standings'] 	= $getKlasementHightlight ? $getKlasementHightlight : '';
		$data["bannerR"] 	= $bannerR ? $bannerR : "";

		/* s: load data to view */
		$view["uri"] 		= "standings";
		$view["title"] 		= "Standings";
		$view["header"] 	= $dataHeader;
		$view["content"] 	= $data;
		$view["footer"] 	= $dataFooter;
		/* e: load data to view */

		$config['content_file'] = "index";
		$config["content_data"] = $view;

		$this->template->initialize($config);
		$this->template->render();
	}

} 