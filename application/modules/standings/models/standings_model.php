<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Standings_model extends MY_Model {

	private $_tabel = 'League';
	private $_klasement = 'Klasement';

	private $_activeStatus = 1;
	
	public function __construct(){
		parent::__construct();
	}

	public function getAllLeagues($where, $id = NULL) 
	{
		$result = $this->db->get_where($this->_tabel, $where);

		if ($result === false) {
			return false;
		}

		return $result->result();
	}

	public function getAllKlasement($where)
	{
		$result = $this->db->get_where($this->_klasement, $where);

		if ($result === false) {
			return false;
		}

		return $result->result();
	}

	public function getStandings()
	{
		$return = [];
		try {
			$this->db->select("
					l.LeagueId,
					l.LeagueName,
					k.KlasementId,
					k.KlasementTeam,
					k.KlasementGame,
					k.KlasementWin,
					k.KlasementDraw,
					k.KlasementLost,
					k.KlasementPlusMinus,
					k.KlasementPoints,
					k.KlasementPosition,
					k.KlasementImg
					", false);
				$this->db->from("{$this->_tabel} l");
				$this->db->join("{$this->_klasement} k", "l.LeagueId = k.KlasementLeague", "LEFT");
				$this->db->where("l.LeagueStatus", $this->_activeStatus);
				$this->db->where("k.KlasementStatus", $this->_activeStatus);
				$this->db->order_by("l.LeagueId, k.KlasementPosition ASC");

			$query = $this->db->get();

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->result();
			foreach ($result as $key => $value) {
				$id 	= $value->LeagueId;
				$idK 	= $value->KlasementId;
				$return[$id]["id"] 		= $value->LeagueId;
				$return[$id]["name"] 	= $value->LeagueName;
				$return[$id]["team"][$idK] 	= [
					"name" 		=> $value->KlasementTeam,
					"game" 		=> $value->KlasementGame,
					"win" 		=> $value->KlasementWin,
					"draw" 		=> $value->KlasementDraw,
					"lost" 		=> $value->KlasementLost,
					"deficit" 	=> $value->KlasementPlusMinus,
					"position" 	=> $value->KlasementPosition,
					"poin" 		=> $value->KlasementPoints,
					"img" 		=> UPLOAD_TEAMS_PATH . "thumb/" . $value->KlasementImg,
				];

				$return[$id]["team"] = array_values($return[$id]["team"]);
			}

			$return = array_values($return);

			return json_decode(json_encode($return), false);
		} catch (Exception $e) {
			
		}
	}

}