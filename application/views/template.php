
<!doctype html>
<html>
<head>
	<!-- s: head -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Jadwal Bola Liga | <?php echo $content_data['title']; ?></title>
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/logo-bola-24-mini.ico"/>
	<!-- e: head -->
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>script/flexslider/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/core.css">
	<script type="text/javascript" src="<?php echo ASSETS_PATH;?>script/jquery-1.10.2.js"></script>
	<?php if (isset($content_data['uri'])) { ?>
		<?php if ($content_data['uri'] == 'standings') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/klasemen.css">
		<?php } else if ($content_data['uri'] == 'schedules') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/hasil.css">
		<?php } else if ($content_data['uri'] == 'newsDetail') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/detail.css">
		<?php } else if ($content_data['uri'] == 'news') { ?>
		<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/news-list.css">
		<?php } ?>
	<?php } ?>
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>style/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_PATH;?>css/fontApis.css">
	<style>
	.paginate .pagination {
		text-align: end;
	}
	.paginate .paginate_button {
		display: inline-block;
		margin: 0 3px;
		text-decoration: underline;
	}

	.paginate .pagination .active {
		text-decoration: none !important;
	}
	</style>
</head>
<body>
	<!-- s: header -->
	<?php echo $this->load->view('header',$content_data['header'],TRUE); ?>
	<!-- e: header -->

	<!-- s: content -->
	<?php echo $this -> load -> view($content_file, $content_data['content']); ?>
	<!-- e: content -->

	<!-- s: footer -->
	<?php echo $this->load->view('footer',$content_data['footer'],TRUE); ?>
	<!-- e: footer -->

	<script type="text/javascript" src="<?php echo ASSETS_PATH;?>script/flexslider/jquery.flexslider.js"></script>
	<script type="text/javascript" src="<?php echo ASSETS_PATH;?>script/core.js"></script>
</body>
</html>
