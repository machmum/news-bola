<header>
	<div class="wrapper">
		<div class="logo"><img src="<?php echo base_url(); ?>assets/img/logo-bola-24-minii.ico"></div>
		<div class="head-ads">head-ads</div>
	</div>
	<nav>
		<div class="wrapper">
			<ul>
				<li><a href="<?php echo base_url();?>"><?php echo $menu; ?></a></li>
				<li><a href="<?php echo base_url();?>standings">klasement</a></li>
				<li><a href="<?php echo base_url();?>schedule">jadwal</a></li>
				<li><a href="<?php echo base_url();?>news">berita</a></li>
				<li><a href="<?php echo base_url();?>index">indeks</a></li>
			</ul>
		</div>
	</nav>
</header>