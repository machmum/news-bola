<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends MY_Model {

	private $_tabel = 'Banner';

	private $_activeStatus = 1;
	
	public function __construct(){
		parent::__construct();
	}

	public function getBanner($where, $limit = "", $offset = "", $id = NULL) 
	{
		try {
			if ($id) {
				$id_value = $id;
				$id_field = 'BannerId';
				$table_name = $this->_tabel;

				$query = $this->read($id_value,$id_field,$table_name);
			} else {
				$this->db->select("*");
				
				foreach ($where as $key => $value) {
					if (is_array($value)) {
						$this->db->where_in($key, $value);
					} else {
						$this->db->where($key, $value);
					}
				}

				// print_r($offset);die;
				if ($limit != "" OR $offset != "") {
					$this->db->limit($limit, $offset);
				}

				$query = $this->db->get($this->_tabel);
			}

			if ($query === false) {
				throw new Exception();
			}

			$result = $query->result();

			return $result;

		} catch (Exception $e) {
			
		}
	}

	/**
	 * Get category banner
	 * @param  [type] $cat [description]
	 * @return [type]      [description]
	 */
	public function getCategoryBanner($cat = NULL) 
	{
		$this->db->where( 'BannerCategory', $cat );
		$this->db->where( 'BannerStatus', $this->_activeStatus);
		$this->db->order_by( 'BannerId','DESC');
		$query = $this->db->get( $this->_tabel );
		return $query->row_array();
	}

}