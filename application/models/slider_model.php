<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_model extends MY_Model {

	protected $_tabel = 'Slider';

	public function __construct(){
		parent::__construct();

	}
	
	public function get($id = NULL) {
		$id_value = $id;
		$id_field = 'SliderId';
		$table_name = $this->_tabel;

		return $this->read($id_value,$id_field,$table_name);
	}
	
	/* s: get data terakhir */
	public function getLatest() {
		$this->db->select('name,id');
		$this->db->from($this->_tabel);
		$this->db->where('status_id',1);
		$this->db->limit(1);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		return $query->row_array();
	}
	/* e: get data terakhir */

	public function getSlider($where, $limit = 100, $offset = 0) {
		try {
			$this->db->select('
				A.SliderId,
				A.SliderName,
				A.SliderImage,
				A.SliderDesc,
				A.SliderURL,
				A.SliderStatus');
			$this->db->from($this->_tabel.' as A');
			
			if ($where <> NULL):
				$this->db->where($where);
			endif;

			if ($limit <> NULL):
				$this->db->limit ( $limit, $offset );
			endif;
			
			$this->db->order_by( 'A.SliderCreatedDate', 'DESC');
			$query = $this->db->get();
			// echo $this->db->last_query(); die();

			if ($query === false) {
				throw new Exception();
			}

			return $query->result();

		} catch (Exception $e) {
			
		}
	}

}

/* End of file Slider_model.php */
/* Location: ./application/models/slider_model.php */