<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Klasement_model extends MY_Model {

	private $_tabel = 'Klasement';

	private $_activeStatus = 1;
	
	public function __construct(){
		parent::__construct();
	}

	public function getAllLeagues($id = NULL) 
	{
		$where = ["Status" => $this->_activeStatus];
		return $this->get_all($this->_tabel,"","",$where);
	}

}