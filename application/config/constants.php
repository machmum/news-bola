<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

define('TOTAL_ITEM_PER_PAGE', 4);
define('TOTAL_NUM_LINKS', 9);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('DS', '/');

define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'] . DS );
define('SITE_PATH', 'http://' . $_SERVER['SERVER_NAME'] . DS );
// define('SITE_PATH','http://'.$_SERVER['SERVER_NAME']);

define('ASSETS_ROOT', SITE_ROOT . 'assets' . DS);
define('ASSETS_PATH', SITE_PATH . 'assets' . DS);

define('UPLOAD_ROOT', SITE_ROOT . 'upload' . DS );
define('UPLOAD_PATH', SITE_PATH . 'upload' . DS );

define('ADMIN_PATH', SITE_PATH . 'admin' . DS);

define('UPLOAD_IMAGE_PATH', ADMIN_PATH . 'assets' . DS . 'img' . DS );
define('UPLOAD_ADMIN_PATH', ADMIN_PATH . 'uploads' . DS);

define('UPLOAD_NEWS_PATH', UPLOAD_ADMIN_PATH .  'news' . DS . 'image' . DS);
define('UPLOAD_COVER_PATH', UPLOAD_ADMIN_PATH .  'cover' . DS . 'image' . DS);
define('UPLOAD_TEAMS_PATH', UPLOAD_ADMIN_PATH .  'klasement' . DS . 'image' . DS);
define('UPLOAD_PLAYER_PATH', UPLOAD_ADMIN_PATH .  'player' . DS . 'image' . DS);
define('UPLOAD_SLIDER_PATH', UPLOAD_ADMIN_PATH .  'slider' . DS . 'image' . DS);
define('UPLOAD_BANNER_PATH', UPLOAD_ADMIN_PATH .  'banner' . DS . 'image' . DS);
define('UPLOAD_PROFILE_PATH', UPLOAD_ADMIN_PATH .  'profile' . DS . 'image' . DS);
// define('UPLOAD_ADMIN_PATH', ADMIN_PATH . 'asuploads' . DS );

define('SHARED_KEY', 'URBANIVAWEB');

/* End of file constants.php */
/* Location: ./application/config/constants.php */