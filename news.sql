/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50639
 Source Host           : localhost:3306
 Source Schema         : news

 Target Server Type    : MySQL
 Target Server Version : 50639
 File Encoding         : 65001

 Date: 06/03/2018 11:39:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Access
-- ----------------------------
DROP TABLE IF EXISTS `Access`;
CREATE TABLE `Access` (
  `AccessId` int(11) NOT NULL AUTO_INCREMENT,
  `AccessModule` int(11) NOT NULL,
  `AccessPrivileges` varchar(100) NOT NULL,
  PRIMARY KEY (`AccessId`),
  KEY `fk_Access_1` (`AccessModule`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Banner
-- ----------------------------
DROP TABLE IF EXISTS `Banner`;
CREATE TABLE `Banner` (
  `BannerId` int(11) NOT NULL AUTO_INCREMENT,
  `BannerName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `BannerDesc` text CHARACTER SET utf8,
  `BannerCode` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT 'R1,R2,HC1,HC2,HB1,HB2,HB3,HB4',
  `BannerImage` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `BannerCategory` smallint(3) DEFAULT NULL,
  `BannerUrl` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `BannerStatus` tinyint(1) DEFAULT '0' COMMENT '0.disabled, 1.enabled',
  `BannerCreatedBy` int(11) DEFAULT NULL,
  `BannerCreatedDate` datetime DEFAULT NULL,
  `BannerModifiedBy` int(11) DEFAULT NULL,
  `BannerModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`BannerId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for Category
-- ----------------------------
DROP TABLE IF EXISTS `Category`;
CREATE TABLE `Category` (
  `CategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryParentId` int(11) DEFAULT NULL,
  `CategoryName` varchar(255) NOT NULL,
  `CategoryUrl` varchar(255) NOT NULL,
  `CategoryDesc` text,
  `CategoryImage` varchar(500) DEFAULT NULL,
  `CategoryStatus` tinyint(4) DEFAULT '1' COMMENT '0. disabled, 1.enabled',
  `CategoryCreatedBy` int(11) NOT NULL,
  `CategoryCreatedDate` datetime NOT NULL,
  `CategoryModifiedBy` int(11) DEFAULT NULL,
  `CategoryModifiedDate` datetime DEFAULT NULL,
  `CategoryOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Image
-- ----------------------------
DROP TABLE IF EXISTS `Image`;
CREATE TABLE `Image` (
  `ImageId` int(11) NOT NULL AUTO_INCREMENT,
  `ImageName` varchar(250) NOT NULL,
  `ImageFile` varchar(500) NOT NULL,
  `ImageCreatedBy` int(11) NOT NULL,
  `ImageCreatedDate` datetime NOT NULL,
  PRIMARY KEY (`ImageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Klasement
-- ----------------------------
DROP TABLE IF EXISTS `Klasement`;
CREATE TABLE `Klasement` (
  `KlasementId` int(11) NOT NULL AUTO_INCREMENT,
  `KlasementTeam` varchar(255) NOT NULL,
  `KlasementGame` int(11) DEFAULT NULL,
  `KlasementWin` int(11) DEFAULT NULL,
  `KlasementDraw` int(11) DEFAULT NULL,
  `KlasementLost` int(11) DEFAULT NULL,
  `KlasementPosition` int(11) DEFAULT NULL,
  `KlasementPlusMinus` int(11) DEFAULT NULL,
  `KlasementPoints` int(11) DEFAULT NULL,
  `KlasementStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0. Disabled, 1.Enabled',
  `KlasementCategory` int(11) DEFAULT NULL,
  `KlasementCreatedBy` int(11) NOT NULL,
  `KlasementCreatedDate` datetime NOT NULL,
  `KlasementModifiedBy` int(11) DEFAULT NULL,
  `KlasementModifiedDate` datetime DEFAULT NULL,
  `KlasementImg` text,
  PRIMARY KEY (`KlasementId`),
  KEY `id, name` (`KlasementId`,`KlasementTeam`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for League
-- ----------------------------
DROP TABLE IF EXISTS `League`;
CREATE TABLE `League` (
  `LeagueId` int(11) NOT NULL,
  `LeagueName` varchar(255) DEFAULT NULL,
  `LeaguaNameInd` varchar(255) DEFAULT NULL,
  `Status` int(2) DEFAULT NULL,
  PRIMARY KEY (`LeagueId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Module
-- ----------------------------
DROP TABLE IF EXISTS `Module`;
CREATE TABLE `Module` (
  `ModuleId` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(255) NOT NULL,
  `ModuleFile` varchar(100) NOT NULL,
  `ModuleControllers` varchar(100) NOT NULL,
  `ModuleDesc` text,
  `ModuleCreatedBy` int(11) NOT NULL,
  `ModuleCreatedDate` datetime NOT NULL,
  `ModuleModifiedBy` int(11) DEFAULT NULL,
  `ModuleModifiedDate` datetime DEFAULT NULL,
  `ModuleStatus` tinyint(4) DEFAULT NULL COMMENT '0.disabled, 1.enabled',
  PRIMARY KEY (`ModuleId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for News
-- ----------------------------
DROP TABLE IF EXISTS `News`;
CREATE TABLE `News` (
  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
  `NewsName` varchar(255) NOT NULL,
  `NewsUrl` varchar(255) NOT NULL,
  `NewsDate` date DEFAULT NULL,
  `NewsTeaser` varchar(500) DEFAULT NULL,
  `NewsTeaserEn` varchar(500) DEFAULT NULL,
  `NewsDesc` text,
  `NewsDescEn` text,
  `NewsImage` varchar(255) DEFAULT NULL,
  `NewsStatus` tinyint(1) DEFAULT NULL COMMENT '1: draft, 2:published, 3:disabled',
  `NewsCreatedBy` int(11) NOT NULL,
  `NewsCreatedDate` datetime NOT NULL,
  `NewsModifiedBy` int(11) DEFAULT NULL,
  `NewsModifiedDate` datetime DEFAULT NULL,
  `NewsPublishedBy` int(11) DEFAULT NULL,
  `NewsPublishedDate` datetime DEFAULT NULL,
  `NewsCategory` int(11) DEFAULT NULL,
  `NewsSetHome` tinyint(1) NOT NULL COMMENT '1.Enable Set Home',
  `NewsMetaKeywords` text,
  `NewsMetaDescriptions` text,
  `NewsTitleOg` text,
  `NewsDescOg` text,
  `NewsImageOg` text,
  `NewsViews` int(11) DEFAULT NULL,
  `RelatedNews` text,
  `RelatedProfile` text,
  PRIMARY KEY (`NewsId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for Role
-- ----------------------------
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(200) NOT NULL,
  `RoleDesc` text,
  `RoleStatus` tinyint(1) DEFAULT NULL COMMENT '0. disabled, 1.enabled',
  `RoleCreatedBy` int(11) NOT NULL,
  `RoleCreatedDate` datetime NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for RoleAccess
-- ----------------------------
DROP TABLE IF EXISTS `RoleAccess`;
CREATE TABLE `RoleAccess` (
  `RoleId` int(11) NOT NULL,
  `AccessId` int(11) NOT NULL,
  KEY `fk_UserAccess_1` (`RoleId`),
  KEY `fk_UserAccess_2` (`AccessId`),
  CONSTRAINT `RoleAccess_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `Role` (`RoleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `RoleAccess_ibfk_2` FOREIGN KEY (`AccessId`) REFERENCES `Access` (`AccessId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Schedule
-- ----------------------------
DROP TABLE IF EXISTS `Schedule`;
CREATE TABLE `Schedule` (
  `ScheduleId` int(11) NOT NULL AUTO_INCREMENT,
  `ScheduleName` varchar(255) NOT NULL,
  `ScheduleTeam1` varchar(200) DEFAULT NULL,
  `ScheduleScore1` tinyint(3) DEFAULT NULL,
  `ScheduleTeam2` varchar(200) DEFAULT NULL,
  `ScheduleScore2` tinyint(3) DEFAULT NULL,
  `ScheduleCategory` int(11) DEFAULT NULL,
  `ScheduleStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0. Disabled, 1.Enabled',
  `ScheduleCreatedBy` int(11) NOT NULL,
  `ScheduleCreatedDate` datetime NOT NULL,
  `ScheduleModifiedBy` int(11) DEFAULT NULL,
  `ScheduleModifiedDate` datetime DEFAULT NULL,
  `ScheduleDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ScheduleId`),
  KEY `id, name` (`ScheduleId`,`ScheduleName`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Seo
-- ----------------------------
DROP TABLE IF EXISTS `Seo`;
CREATE TABLE `Seo` (
  `SeoId` int(11) NOT NULL AUTO_INCREMENT,
  `SeoName` varchar(255) NOT NULL,
  `SeoMetaDesc` text,
  `SeoMetaKeywords` text,
  `SeoTitle` text,
  `SeoDesc` text,
  `SeoImage` text,
  `SeoSegment` varchar(500) NOT NULL,
  `SeoStatus` tinyint(1) NOT NULL COMMENT '0. disabled, 1.enabled',
  `SeoCreatedBy` int(11) NOT NULL,
  `SeoCreatedDate` datetime NOT NULL,
  `SeoModifiedBy` int(11) DEFAULT NULL,
  `SeoModifiedDate` datetime NOT NULL,
  PRIMARY KEY (`SeoId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for Slider
-- ----------------------------
DROP TABLE IF EXISTS `Slider`;
CREATE TABLE `Slider` (
  `SliderId` int(11) NOT NULL AUTO_INCREMENT,
  `SliderName` varchar(255) DEFAULT NULL,
  `SliderDesc` varchar(255) DEFAULT NULL,
  `SliderImage` varchar(300) DEFAULT NULL,
  `SliderURL` varchar(500) DEFAULT NULL,
  `SliderCategory` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1. Desktop, 2. Mobile',
  `SliderStatus` tinyint(1) DEFAULT '0' COMMENT '0.disabled, 1.enabled',
  `SliderCreatedBy` int(11) DEFAULT NULL,
  `SliderCreatedDate` datetime DEFAULT NULL,
  `SliderModifiedBy` int(11) DEFAULT NULL,
  `SliderModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SliderId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for UserLogin
-- ----------------------------
DROP TABLE IF EXISTS `UserLogin`;
CREATE TABLE `UserLogin` (
  `UserLoginId` int(11) NOT NULL AUTO_INCREMENT,
  `UserLoginEmail` varchar(150) NOT NULL,
  `UserLoginPass` varchar(255) NOT NULL,
  `UserLoginName` varchar(255) NOT NULL,
  `UserLoginRole` int(11) NOT NULL,
  `UserLoginStatus` tinyint(4) DEFAULT '1' COMMENT '0. disaled, 1.enabled',
  `UserLoginCreatedBy` varchar(45) NOT NULL,
  `UserLoginCreatedDate` datetime NOT NULL,
  `UserLoginModifiedBy` int(11) DEFAULT NULL,
  `UserLoginModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserLoginId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
