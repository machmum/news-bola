$('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 300,
        itemMargin: 3,
        pausePlay: false,
        controlNav: false,
        slideshow: false,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });

$(window).load(function() {
  $('.flexslider99').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: true,
  });
});

$(window).load(function() {
  $('.flexslider-news').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: true,
  });
});

$('.filter-content a').click(function(){

  //Expand or collapse this panel
  $(this).next().slideToggle('fast');
  $(this).toggleClass('active');

  //Hide the other panels
  $(".filter-content div").not($(this).next()).slideUp('fast');
  $('.filter-content a').not($(this)).removeClass('active');

});

$('.filter-title span').click(function(){
    $('.filter').toggleClass('active');
});